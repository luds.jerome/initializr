FROM openjdk:18-ea-11-jdk-alpine3.13

ENV _JAVA_OPTIONS "-Xms256m -Xmx512m -Djava.awt.headless=true"

ARG JAR_FILE=*.jar
ADD target/${JAR_FILE} app.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "/app.jar"]